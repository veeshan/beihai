<!-- docs/_sidebar.md -->

- 基本规范
  - [弹性通信协议](common/fcp.md)
  - [安全规范](common/security.md)

- 部署流程
  - [Make Borne](deploy/borne.md)
  - [Make Plane](deploy/plane.md)
  - [Make Escort](deploy/escort.md)
